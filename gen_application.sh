#!/bin/bash
LOCAL_DIR="~/.local/share/applications"
SYSTEMD_DIR="/usr/share/applications"

function generate_config() {
# filename, name, description, icon_path, bash_command, is_terminal
cat > "$1" << EOF
[Desktop Entry]
Type=Application
Encoding=UTF-8
Name=$2
Comment=$3
Icon=$4
Exec=$5
Terminal=$6
Categories=Tags;Describing;Application
EOF
}

read -p 'Application short_name (without .desktop): ' short_name
read -p 'Application full name: ' name
read -p 'Description: ' description
read -p 'Icon Path: ' icon_path
read -p 'Command: ' bash_command
read -p 'Show terminal (true, false): ' is_terminal

generate_config "$short_name.desktop" "$name" "$description" "$icon_path" "$bash_command" "$is_terminal"

echo -e "\nrun this to move application:\nsudo mv $short_name.desktop $SYSTEMD_DIR/$short_name.desktop\nor"
echo -e "sudo mv $short_name.desktop $LOCAL_DIR/$short_name.desktop"