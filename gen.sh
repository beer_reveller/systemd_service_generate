#!/bin/bash
SYSTEMD_DIR="/etc/systemd/system"

function generate_config() {
# path, description, wd, bash_command, user
cat > "$1" << EOF
[Unit]
Description=$2
After=multi-user.target

[Service]
Type=simple
User=$5
Restart=always
WorkingDirectory=$3
ExecStart=$4

[Install]
WantedBy=multi-user.target
EOF
}

read -p 'Service name without(.service): ' service_name
read -p 'Description: ' description
read -p 'User: ' user
read -p 'Working Directory: ' wd
read -p 'Command: ' bash_command

generate_config "$service_name.service" "$description" "$wd" "$bash_command" "$user"

echo -e "\nrun this to move service:\nsudo mv $service_name.service $SYSTEMD_DIR/$service_name.service\n"